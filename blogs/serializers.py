from django.contrib.auth.models import User
from rest_framework import serializers
from rest_framework.serializers import SlugRelatedField
from blogs.models import Blogs, Category


class AllSerializer(serializers.ModelSerializer):
    category = SlugRelatedField(queryset=Category.objects.all(), slug_field='cat_name')
    author = SlugRelatedField(queryset=User.objects.all(), slug_field='username')

    class Meta:
        model = Blogs
        fields = ['id', 'name', 'category', 'text', 'author', 'image', 'date']
