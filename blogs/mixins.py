from rest_framework.response import Response


class CustomMixin:
    def get_all_data(self, request):
        queryset = self.filter_queryset(self.get_queryset())
        serializer_class = self.get_serializer_class()
        blogs_response = serializer_class(queryset, many=True)
        return Response({'Blogs': blogs_response.data})




