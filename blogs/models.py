from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    cat_name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.cat_name)


class Blogs(models.Model):
    name = models.CharField(max_length=80)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    text = models.TextField(blank=True)
    author = models.ForeignKey(User, on_delete=models.SET_NULL, default=None, null=True)
    date = models.DateTimeField(auto_now_add=True)
    edit_date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(default='blog.png', blank=True)

    def __str__(self):
        return f'{self.name} - {self.author} - {self.category} - {self.date}'


