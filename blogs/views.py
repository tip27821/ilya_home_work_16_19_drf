from rest_framework.generics import GenericAPIView
from blogs.models import Blogs
from blogs.serializers import AllSerializer
from blogs.mixins import CustomMixin
import datetime


class GetAllDataApiView(CustomMixin, GenericAPIView):
    queryset = Blogs.objects.all()
    serializer_class = AllSerializer

    def get(self, request):
        return self.get_all_data(request)


class NewGetAllData(GetAllDataApiView):
    last_five_days = datetime.datetime.today() - datetime.timedelta(days=5)
    queryset = Blogs.objects.filter(date__gte=last_five_days)
